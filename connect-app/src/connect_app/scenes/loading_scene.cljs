(ns connect-app.scenes.loading-scene
  (:require [connect-app.utils.react-native :as rn]))

(def styles {:container {:flex 1 :alignItems "center" :justifyContent "center"}})

(defn container []
  [rn/view {:style (:container styles)}
   [rn/activity-indicator {:size "large"}]])
