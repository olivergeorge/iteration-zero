(ns connect-app.scenes.navigation-scene
  (:require [connect-app.scenes.loading-scene :as loading-scene]
            [connect-app.scenes.signin-scene :as signin-scene]
            [connect-app.scenes.welcome-scene :as welcome-scene]
            [connect-app.utils.navigation-utils :as navigation-utils]
            [reagent.core :as r]
            [re-frame.core :as re-frame]))


(def AppRouteConfigs
  #js {:Welcome #js {:screen            (r/reactify-component welcome-scene/container)
                     :navigationOptions #js {:title "Welcome"}}})

(def AuthRouteConfigs
  #js {:SignIn #js {:screen            (r/reactify-component signin-scene/container)
                    :navigationOptions #js {:title "Sign In"}}})

(def RootStack
  (navigation-utils/createSwitchNavigator
    #js {:AuthLoading #js {:screen            (r/reactify-component loading-scene/container)
                           :navigationOptions #js {:title "Loading"}}
         :App         (navigation-utils/createStackNavigator AppRouteConfigs)
         :Auth        (navigation-utils/createStackNavigator AuthRouteConfigs)}
    #js {:initialRouteName "AuthLoading"}))

(defn handle-navigator-ref [navigator]
  (navigation-utils/init navigator)
  (re-frame/dispatch [:app/navigator-ready]))

(defn container []
  [:> RootStack {:ref handle-navigator-ref}])
