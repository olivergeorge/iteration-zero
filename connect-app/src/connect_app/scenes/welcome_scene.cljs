(ns connect-app.scenes.welcome-scene
  (:require [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [connect-app.utils.react-native :as rn]
            [connect-app.images :as images]))

(def styles
  {:container        {:flex-direction "column" :margin 40 :align-items "center"}
   :greeting-text    {:font-size 30 :font-weight "100" :margin-bottom 20 :text-align "center"}
   :logo-image       {:width 80 :height 80 :margin-bottom 30}
   :button-container {:background-color "#999" :padding 10 :border-radius 5}
   :button-text      {:color "white" :text-align "center" :font-weight "bold"}})

(defn alert [title]
  (.alert rn/Alert title))

(defn container []
  (let [greeting (subscribe [:app/get-greeting])]
    (fn []
      [rn/view {:style (:container styles)}
       [rn/text {:style (:greeting-text styles)} @greeting]
       [rn/image {:source images/logo-img
                  :style  (:logo-image styles)}]
       [rn/touchable-highlight {:style    (:button-container styles)
                                :on-press #(dispatch [:auth/logout])}
        [rn/text {:style (:button-text styles)} "Sign out"]]])))
