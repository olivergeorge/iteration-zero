(ns connect-app.interceptors
  (:require [cljs.spec.alpha :as s]
            [connect-app.db :as db]
            [re-frame.core :as re-frame]))


(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db [event]]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check after " event " failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (re-frame/after (partial check-and-throw ::db/app-db))
    []))

(def log-effects
  (re-frame/->interceptor
    :id ::after
    :after (fn [ctx]
             (doseq [[k v] (re-frame/get-effect ctx)
                     :when (not= k :db)]
               (js/console.log "Effect: " (str k) (str v)))
             ctx)))

(def std-ins
  (if goog.DEBUG [re-frame/debug log-effects validate-spec] []))
