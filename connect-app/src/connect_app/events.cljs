(ns connect-app.events
  (:require
    [clojure.spec.alpha :as s]
    [re-frame.core :as re-frame]
    [connect-app.db :refer [app-db]]
    [connect-app.interceptors :as ins]))


(re-frame/reg-event-db
  :app/initialize-db
  ins/std-ins
  (fn [_ _]
   app-db))

(re-frame/reg-event-db
  :app/set-greeting
  ins/std-ins
  (fn [db [_ value]]
   (assoc db :greeting value)))

(re-frame/reg-event-fx
  :app/navigator-ready
  ins/std-ins
  (fn [_ _]
    {:dispatch [:auth/load]
     :splash-screen/hide nil}))