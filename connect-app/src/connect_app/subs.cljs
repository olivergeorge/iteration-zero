(ns connect-app.subs
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
  :app/get-greeting
  (fn [db _]
    (:greeting db)))