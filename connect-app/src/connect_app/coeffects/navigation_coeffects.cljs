(ns connect-app.coeffects.navigation-coeffects
  (:require [connect-app.utils.navigation-utils :as navigation-utils]
            [re-frame.core :as re-frame]))

(re-frame/reg-cofx :navigator/state navigation-utils/state)
