(ns connect-app.constants.api-constants)

(def azure-authority "https://login.windows.net/markcondensecom.onmicrosoft.com")
(def azure-appid "ae86dd6b-486a-4c00-9555-35c182ce4921")
(def azure-redirect-uri "https://connect.condense.com.au/UtasBackend/")
(def azure-graph-base-url "https://graph.windows.net")
