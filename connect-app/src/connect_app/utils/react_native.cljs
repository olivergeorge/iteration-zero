(ns connect-app.utils.react-native
  (:require [reagent.core :as r]))

(def react-native (js/require "react-native"))

(def Alert (.-Alert react-native))
(def text (r/adapt-react-class (.-Text react-native)))
(def view (r/adapt-react-class (.-View react-native)))
(def image (r/adapt-react-class (.-Image react-native)))
(def touchable-highlight (r/adapt-react-class (.-TouchableHighlight react-native)))
(def activity-indicator (r/adapt-react-class (.-ActivityIndicator react-native)))

(def platform (.-Platform react-native))
(def ios? (= "ios" (.-OS platform)))
(def android? (= "android" (.-OS platform)))