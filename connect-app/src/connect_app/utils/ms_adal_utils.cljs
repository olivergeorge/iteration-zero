(ns connect-app.utils.ms-adal-utils)

(def react-native-ms-adal (js/require "react-native-ms-adal"))
(def MSAdalLogin (.-MSAdalLogin react-native-ms-adal))
(def MSAdalLogout (.-MSAdalLogout react-native-ms-adal))
(def getValidMSAdalToken (.-getValidMSAdalToken react-native-ms-adal))
