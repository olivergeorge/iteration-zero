(ns connect-app.events.auth-events
  (:require [re-frame.core :as re-frame]
            [connect-app.interceptors :as ins]))

(re-frame/reg-event-fx
  :auth/load
  [ins/std-ins]
  (fn [_ _]
    {:storage/get-item   {:key "auth" :on-success [:auth.load/success] :on-failure [:auth.load/failure]}
     :navigator/navigate {:routeName "AuthLoading"}}))

(re-frame/reg-event-fx
  :auth.load/success
  [ins/std-ins]
  (fn [{:keys [db]} [_ data]]
    {:db                 (assoc db :auth data)
     :navigator/navigate {:routeName "App"}}))

(re-frame/reg-event-fx
  :auth.load/failure
  [ins/std-ins]
  (fn [_ _]
    {:navigator/navigate {:routeName "Auth"}}))

(re-frame/reg-event-fx
  :auth/login
  [ins/std-ins]
  (fn [{:keys [db]} _]
    {:db                 (assoc db :auth {:status :auth/login})
     :ms-adal/login      {:success-v [:auth.login/success] :error-v [:auth.login/error]}
     :navigator/navigate {:routeName "AuthLoading"}}))

(re-frame/reg-event-fx
  :auth.login/success
  [ins/std-ins]
  (fn [{:keys [db]} ev]
    (let [data {:status :auth.login/success}]
      {:db                 (assoc db :auth data)
       :navigator/navigate {:routeName "App"}
       :storage/set-item   {:key "auth" :data data}})))

(re-frame/reg-event-fx
  :auth.login/error
  [ins/std-ins]
  (fn [{:keys [db]} [_ error]]
    {:db                 (assoc db :auth {:status :auth.login/error :error error})
     :navigator/navigate {:routeName "Auth"}}))

(re-frame/reg-event-fx
  :auth/logout
  [ins/std-ins]
  (fn [{:keys [db]} _]
    {:db                 (assoc db :auth {:status :auth/logout})
     :ms-adal/logout     {:success-v [:auth.logout/success]
                          :error-v   [:auth.logout/error]}
     :navigator/navigate {:routeName "AuthLoading"}}))

(re-frame/reg-event-fx
  :auth.logout/success
  [ins/std-ins]
  (fn [{:keys [db]} _]
    {:db                  (assoc db :auth {:status :auth.logout/success})
     :navigator/navigate  {:routeName "Auth"}
     :storage/remove-item {:key "auth"}}))

(re-frame/reg-event-fx
  :auth.logout/error
  [ins/std-ins]
  (fn [{:keys [db]} [_ error]]
    {:db                 (assoc db :auth {:status :auth.logout/error :error error})
     :navigator/navigate {:routeName "App"}}))
