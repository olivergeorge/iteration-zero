(ns connect-app.effects.splash-screen-effects
  (:require [re-frame.core :as re-frame]))

(def SplashScreen (.-default (js/require "react-native-splash-screen")))

(re-frame/reg-fx
  :splash-screen/hide
  (fn []
    (.hide SplashScreen)))
