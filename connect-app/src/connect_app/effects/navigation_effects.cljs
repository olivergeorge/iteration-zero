(ns connect-app.effects.navigation-effects
  (:require [re-frame.core :as re-frame]
            [connect-app.utils.navigation-utils :as navigation-utils]))

(re-frame/reg-fx :navigator/navigate navigation-utils/navigate)
(re-frame/reg-fx :navigator/back navigation-utils/back)
(re-frame/reg-fx :navigator/push navigation-utils/push)
(re-frame/reg-fx :navigator/pop navigation-utils/pop)
