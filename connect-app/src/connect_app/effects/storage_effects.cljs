(ns connect-app.effects.storage-effects
  (:require [cljs.reader :refer [read-string]]
            [re-frame.core :as re-frame]))


(def AsyncStorage (.-AsyncStorage (js/require "react-native")))


(defn dispatch [ev & args]
  (when ev
    (re-frame/dispatch (into ev args))))

(defn set-item
  [{:keys [key value on-success on-failure]}]
  (.setItem AsyncStorage (pr-str key) (pr-str value)
            (fn [error]
              (if-not error
                (dispatch on-success)
                (dispatch on-failure {:type :fault :error error})))))


(defn get-item
  [{:keys [key on-success on-failure]}]
  (.getItem AsyncStorage (pr-str key)
            (fn [error result]
              (cond error
                    (dispatch on-failure {::type :fault :error error})

                    (nil? result)
                    (dispatch on-failure {::type :unavailable})

                    (string? result)
                    (try (dispatch on-success (read-string result))
                         (catch :default e
                           (dispatch on-failure {::type :fault :error e})))

                    :else
                    (dispatch on-failure {::type :fault :error "Unexpected result type" :result result})))))

(defn remove-item
  [{:keys [key on-success on-failure]}]
  (.removeItem AsyncStorage (pr-str key)
               (fn [error]
                 (if-not error
                   (dispatch on-success)
                   (dispatch on-failure {:type :fault :error error})))))

(re-frame/reg-fx :storage/set-item set-item)
(re-frame/reg-fx :storage/get-item get-item)
(re-frame/reg-fx :storage/remove-item remove-item)
