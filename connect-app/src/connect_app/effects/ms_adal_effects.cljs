(ns connect-app.effects.ms-adal-effects
  (:require [re-frame.core :as re-frame]
            [connect-app.constants.api-constants :as api-constants]
            [connect-app.utils.ms-adal-utils :as api-utils]))


(re-frame/reg-fx
  :ms-adal/login
  (fn [{:keys [success-v error-v]}]
    (-> (api-utils/MSAdalLogin api-constants/azure-authority
                               api-constants/azure-appid
                               api-constants/azure-redirect-uri
                               api-constants/azure-graph-base-url)
        (.then #(re-frame/dispatch success-v))
        (.catch #(re-frame/dispatch (conj error-v (str %)))))))

(re-frame/reg-fx
  :ms-adal/logout
  (fn [{:keys [success-v error-v]}]
    (-> (api-utils/MSAdalLogout api-constants/azure-authority
                                api-constants/azure-redirect-uri)
        (.then #(re-frame/dispatch success-v))
        (.catch #(re-frame/dispatch (conj error-v %))))))
