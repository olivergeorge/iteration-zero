(ns connect-app.ios.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [connect-app.events]
            [connect-app.subs]
            [connect-app.scenes.navigation-scene :as navigation-scene]
            [connect-app.effects.navigation-effects]
            [connect-app.coeffects.navigation-coeffects]
            [connect-app.effects.storage-effects]
            [connect-app.events.auth-events]
            [connect-app.effects.ms-adal-effects]
            [connect-app.effects.splash-screen-effects]))

(def react-native (js/require "react-native"))

(def AppRegistry (.-AppRegistry react-native))

(def app-root navigation-scene/container)

(defn init []
      (dispatch-sync [:app/initialize-db])
      (.registerComponent AppRegistry "ConnectApp" #(r/reactify-component app-root)))
